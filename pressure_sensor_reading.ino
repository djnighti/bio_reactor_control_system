const int pressureInput = A0; //select the analog input pin for the pressure transducer
const int pressureZero = 102.4; //analog reading of pressure transducer at 0psi
const int pressureMax = 921.6; //analog reading of pressure transducer at 10psi
const int pressuretransducermaxPSI = 10; //psi value of transducer being used
const int baudRate = 9600; //constant integer to set the baud rate for serial monitor
const int sensorreadDelay = 250; //constant integer to set the sensor read delay in milliseconds

float pressureValue = 0; //variable to store the value coming from the pressure transducer

void setup()
{
  Serial.begin(baudRate); //initializes serial communication at set baud rate bits per second
  delay(3000);
}

void loop()
{
  pressureValue = analogRead(pressureInput); //reads value from input pin and assigns to variable
  pressureValue = ((pressureValue - pressureZero) * pressuretransducermaxPSI) / (pressureMax - pressureZero); //conversion equation to convert analog reading to psi
  Serial.print("pressureValue.val="); //prints value from previous line to serial
  Serial.print(int(pressureValue * 1000)); //can only send integers, must multiply 1000 and make sure it is type int
  Serial.write(0xff);
  Serial.write(0xff);
  Serial.write(0xff);

  if (Serial.available() > 0) {
    char Received = Serial.read();
    if (Received == 's'); {
      int rTarget = Serial.parseInt();
      Serial.print("rTarget.val="); //prints value from previous line to serial
      Serial.print(rTarget); //can only send integers, must multiply 1000 and make sure it is type int
      Serial.write(0xff);
      Serial.write(0xff);
      Serial.write(0xff);
    }
  }
  delay(sensorreadDelay); //delay in milliseconds between read values
}
