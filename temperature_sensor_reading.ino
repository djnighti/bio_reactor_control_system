#include <OneWire.h>
#include <DallasTemperature.h>

const int temperatureInput = 7; // digital pin 7
OneWire oneWire(temperatureInput);
float temperatureValue = 0; //variable to store the value coming from the temperature sensor
int temperatureValueInt = temperatureValue * 1000;
const int temperatureRoomInt = 32 * 1000; 
const int temperatureMaxInt = 80 * 1000;
const int baudRate = 9600; //constant integer to set the baud rate for serial monitor
const int sensorreadDelay = 250; //constant integer to set the sensor read delay in milliseconds


// GUI variables
String temperatureValueString = "tempValue.val=";
int temperatureWaveformID = 12;
int temperatureValueChannel = 0;
int temperatureRoomChannel = 1;
int temperatureMaxChannel = 2;
float Celsius = 0;
DallasTemperature temperature_sensor(&oneWire);

void setup() {
  temperature_sensor.begin();
  Serial.begin(baudRate);
}

void loop() {
  temperature_sensor.requestTemperatures();

  Celsius = temperature_sensor.getTempCByIndex(0);
  temperatureValueInt = int(Celsius*1000);
  
  writeToByte(temperatureValueString, temperatureValueInt);
  value_to_waveform(temperatureWaveformID, temperatureValueInt  , temperatureValueChannel);
  value_to_waveform(temperatureWaveformID, temperatureRoomInt, temperatureRoomChannel);
  value_to_waveform(temperatureWaveformID, temperatureMaxInt, temperatureMaxChannel);
  delay(sensorreadDelay);
}

void writeToByte(String display_string, int display_value) {
  Serial.print(display_string); //prints value from previous line to serial
  Serial.print(display_value); //can only send integers, must multiply 1000 and make sure it is type int
  Serial.write(0xff);
  Serial.write(0xff);
  Serial.write(0xff);
}

void value_to_waveform(int object_id, int val_wave, int channel)
{
  String Tosend = "add ";
  Tosend += object_id;
  Tosend += ",";
  Tosend += channel;
  Tosend += ",";
  Tosend += val_wave;
  Serial.print(Tosend);
  Serial.write(0xff);
  Serial.write(0xff);
  Serial.write(0xff);
}
