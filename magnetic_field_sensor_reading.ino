#include "Adafruit_MLX90393.h"

// Magnetometer
Adafruit_MLX90393 magnetometer_sensor = Adafruit_MLX90393();
#define MLX90393_CS 10
int magneticValueInt = 0;
const int magneticZero = 0; 
const int magneticMaxInt = 20 * 1000;
const int baudRate = 9600; //constant integer to set the baud rate for serial monitor
const int sensorreadDelay = 250; //constant integer to set the sensor read delay in milliseconds

// GUI variables
String magneticValueString = "magneticValue.val=";
int magneticWaveformID = 11;
int magneticValueChannel = 0;
int magneticZeroChannel = 1;
int magneticMaxChannel = 2;

void setup(void)
{
  Serial.begin(baudRate);
  magnetometer_sensor.begin_I2C(); // Begin I2C comm
  magnetometer_sensor.setGain(MLX90393_GAIN_2_5X); // Set gain
  magnetometer_sensor.setResolution(MLX90393_X, MLX90393_RES_19);
  magnetometer_sensor.setResolution(MLX90393_Y, MLX90393_RES_19);
  magnetometer_sensor.setResolution(MLX90393_Z, MLX90393_RES_16);
  magnetometer_sensor.setOversampling(MLX90393_OSR_2); // Set oversampling
  magnetometer_sensor.setFilter(MLX90393_FILTER_6); // Set digital filtering
}
 
void loop(void) {
  float z;
  sensors_event_t event;
  magnetometer_sensor.getEvent(&event);
  magneticValueInt = int(event.magnetic.z * 1000);
  
  writeToByte(magneticValueString, magneticValueInt);
  value_to_waveform(magneticWaveformID, magneticValueInt  , magneticValueChannel);
  value_to_waveform(magneticWaveformID, magneticZero, magneticZeroChannel);
  value_to_waveform(magneticWaveformID, magneticMaxInt, magneticMaxChannel);
  delay(sensorreadDelay);
}

void writeToByte(String display_string, int display_value) {
  Serial.print(display_string); //prints value from previous line to serial
  Serial.print(display_value); //can only send integers, must multiply 1000 and make sure it is type int
  Serial.write(0xff);
  Serial.write(0xff);
  Serial.write(0xff);
}

void value_to_waveform(int object_id, int val_wave, int channel)
{
  String Tosend = "add ";
  Tosend += object_id;
  Tosend += ",";
  Tosend += channel;
  Tosend += ",";
  Tosend += val_wave;
  Serial.print(Tosend);
  Serial.write(0xff);
  Serial.write(0xff);
  Serial.write(0xff);
}
