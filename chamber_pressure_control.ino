// Pressure sensor
const int pressureInput = A0; //select the analog input pin for the pressure transducer
const int pressureMin = 102.4; //analog reading of pressure transducer at 0psi
const int pressureMax = 921.6; //analog reading of pressure transducer at 10psi
const int pressureRange = pressureMax - pressureMin; //analog reading of pressure transducer at 10psi
const int pressuretransducermaxPSI = 10; //psi value of transducer being used
const int baudRate = 9600; //constant integer to set the baud rate for serial monitor
const int sensor_read_delay = 250; //constant integer to set the sensor read delay in milliseconds
const int start_up_delay = 200; //constant integer to set the sensor read delay in milliseconds
float sensor_analog_value = 0; //variable to store the value coming from the pressure transducer
float sensor_psi_value;
float target_analog_value;

// Pump (DC Motor)
int direction_pin = 7;
int pwm_pin = 6;

// control
int motor_direction = 0;
float control_signal;
int pressure = 0;
long previous_time = 0.0;
int previous_error = 0;
float integral_error = 0;
float default_target_psi_value = 0.29;
float default_target_analog_value = (default_target_psi_value * pressureRange / pressuretransducermaxPSI) + pressureMin; //conversion equation to convert psi to analog reading
float threshold = 0.05 * default_target_psi_value;
unsigned long seconds_to_miliseconds = 1000;
int time_to_pressurize = 50;

// Relays
const int relayPin_1 = 8;
const int relayPin_2 = 9;


// PID constants (Default)
float kp = 5.0;
float kd = 1.0;
float ki = 6.5;

// Display
String display_string;
int display_value;
int initializeSetup = 0;
int display_converter = 1000;

void setup() {
  Serial.begin(baudRate); // Set Baud rate

  pinMode(direction_pin, OUTPUT); // Set the direction pin output
  pinMode(pwm_pin, OUTPUT); // Set the pwm pin output
  writeToByte("rTarget.val=", int(default_target_psi_value * display_converter));
  writeToByte("targetVal.val=", int(default_target_psi_value * display_converter));


  digitalWrite(relayPin_1, HIGH);
  digitalWrite(relayPin_2, HIGH);
  pinMode(relayPin_1, OUTPUT);
  pinMode(relayPin_2, OUTPUT);

  while (initializeSetup < 100) {
    initializeSetup += 20;
    writeToByte("progressBar.val=", initializeSetup);
    delay(start_up_delay);
  }
}

void loop() {
  if (millis() < time_to_pressurize * seconds_to_miliseconds) {

    //  Send pressure sensor readings to display
    sensor_analog_value = analogRead(pressureInput); //reads value from input pin and assigns to variable
    sensor_psi_value = ((sensor_analog_value - pressureMin) * pressuretransducermaxPSI) / (pressureRange); //conversion equation to convert analog reading to psi
    writeToByte("pressureValue.val=", int(sensor_psi_value * display_converter));
    target_analog_value = default_target_analog_value;

    if (Serial.available() > 0) {
      char Received = Serial.read();
      if (Received == 's') {
        int rTarget = Serial.parseInt();
        target_analog_value = ((rTarget / display_converter) * pressureRange / pressuretransducermaxPSI) + pressureMin; //conversion equation to convert psi to analog reading
        writeToByte("rTarget.val=", int(rTarget * display_converter));
      }
      else if (Received == 'd') {
        writeToByte("rTarget.val=", int(default_target_psi_value * display_converter));
        writeToByte("targetVal.val=", 0);
        float target_analog_value = default_target_analog_value;
        //      }
        //      else if (Received == 'a') {
        //        int state = Serial.parseInt();
        //        digitalWrite(relayPin_1, state);
        //      }
        //      else if (Received == 'b') {
        //        int state = Serial.parseInt();
        //        digitalWrite(relayPin_2, state);
        //      }
      }
      else if (Received == 'a') {
        digitalWrite(relayPin_1, LOW);
      }
      else if (Received == 'A') {
        digitalWrite(relayPin_1, HIGH);
      }
      else if (Received == 'b') {
        digitalWrite(relayPin_2, LOW);
      }
      else if (Received == 'B') {
        digitalWrite(relayPin_2, HIGH);
      }
    }


    // time difference
    long current_time = micros();
    long delt_time = ((float)(current_time - previous_time)) / 1.0e6;
    previous_time = current_time;

    // kp
    int error = target_analog_value - sensor_analog_value;

    // kd
    float de_dt = (error - previous_error) / delt_time;

    // ki
    integral_error = integral_error + error * delt_time;

    // control signal
    float u = kp * error + kd * de_dt + ki * integral_error;

    control_signal = fabs(u);
    if (control_signal > 255) {
      control_signal = 255;
    }

    if (u < 0) {
      control_signal = 0;
    }
    if (abs(error) <= threshold) {
      delay(500);
      control_signal = 0;
    }

    // send control signal
    analogWrite(pwm_pin, control_signal);
    digitalWrite(direction_pin, motor_direction);


    //  Print to LCD
    sensor_psi_value = ((sensor_analog_value - pressureMin) * pressuretransducermaxPSI) / (pressureRange); //conversion equation to convert analog reading to psi
    delay(sensor_read_delay); //delay in milliseconds between read values
  }
  else {
    control_signal = 0;
    analogWrite(pwm_pin, control_signal);
    delay(start_up_delay);
  }
  //  lcd.clear();
}

void writeToByte(String display_string, int display_value) {
  Serial.print(display_string); //prints value from previous line to serial
  Serial.print(display_value); //can only send integers, must multiply 1000 and make sure it is type int
  Serial.write(0xff);
  Serial.write(0xff);
  Serial.write(0xff);
}
