# Bioreactor Control System

## Table of Contents
  - [**Hardware Requirements**](#hardware-requirements)
    - [Single Board Computer](#single-board-computer)
    - [Power Supplies](#power-supplies)
    - [Display](#display)
    - [Motor Shield](#motor-shield)
    - [Air Compressor](#air-compressor)
    - [Water Pump](#water-pump)
    - [Relay Module](#relay-module)
    - [Sensors](#Sensors)
  - [**Bioreactor Control System Program Library**](#bioreactor-control-system)
    - [All Systems Ready](#all-systems-ready)
      - [bioreactor_control_system](#bio_reactor_control_system)
    - [Subsystems Testing](#subsystems-testing)
      - [Motor Shield And Pumps](#motor-shield-and-pumps)
        - [adafruit_motor_shield](#adafruit_motor_shield)
        - [chamber_pressure_control](#chamber_pressure_control)
        - [chamber_flow_control](#chamber_flow_control)
      - [Sensor Readings](#sensors)
        - [data_logger](#data_logger)
        - [pressure_sensor_reading](#pressure_sensor_reading)
        - [temperature_sensor_reading](#temperature_sensor_reading)
        - [magnetic_field_sensor_reading](#magnetic_field_sensor_reading)
      - [Relay Control](#relay_control)
        - [relay_control](#relay_control)
  - [**GUI**](#gui)
    - [Pages](#pages)
      - [Home](#home-page)
      - [Health](#health-page)
      - [Settings](#settings-page)
        - [Chamber Pressure Settings](#settings-pressure-page)
        - [Flow Rate Settings](#settings-shear-stress-page)
        - [Magnetic Field Settings](#settings-magnetic-field-page)
    - [Communication](#Communication)
      - [How To Send Messages](#how-to-send-messages)
    - [How To Make Changes To GUI](#how-to-make-changes-to-gui)

# **Dependencies**

The Following list of libraries are essential to be used in [bioreactor_control_system](#bio_reactor_control_system) as well their respective [Subsystems Testing](#subsystems-testing) programs.

- OneWire
- DallasTemperature
- Adafruit_MLX90393
- Wire
- Adafruit_MotorShield
- utility/Adafruit_MS_PWMServoDriver
- Nextion

# **Hardware Requirements**

#### **Single Board Computer**

Arduino Mega 2560

#### **Power Supplies**

- 12V 5A DC source
- 12V 5A DC source

#### **Display**

Nextion 5in resistive touch display found <a href="https://www.amazon.com/WIshioT-NX8048T050-Intelligent-Resistive-Raspberry/dp/B07B4DV86Z/ref=sr_1_1_sspa?dchild=1&keywords=Nextion+Display+5&qid=1618699159&s=electronics&sr=1-1-spons&psc=1&spLa=ZW5jcnlwdGVkUXVhbGlmaWVyPUE5SFBIM0xMSjdXVjMmZW5jcnlwdGVkSWQ9QTAzNzA0MjgzU09EMzdBNzFIOVI1JmVuY3J5cHRlZEFkSWQ9QTA5NjMxNzlNNkk3WTdRVzVISDYmd2lkZ2V0TmFtZT1zcF9hdGYmYWN0aW9uPWNsaWNrUmVkaXJlY3QmZG9Ob3RMb2dDbGljaz10cnVl" >here</a>

#### **Air Compressor**

12V Brushed DC motor pump found <a href="https://www.amazon.com/KPM27H-Mini-Diaphragm-Type-300mA/dp/B07DLKNSJ2/ref=pd_di_sccai_20?pd_rd_w=g4l7J&pf_rd_p=c9443270-b914-4430-a90b-72e3e7e784e0&pf_rd_r=GSZ5Q2VDDK2NPQH7G0XP&pd_rd_r=3fe9ea14-4ea9-4130-a401-3b9cdb8c07f6&pd_rd_wg=tVisG&pd_rd_i=B07DLKNSJ2&psc=1" >here</a>

#### **Water Pump**

12V Brushed DC motor peristaltic pump found <a href="https://www.amazon.com/Gikfun-Peristaltic-Connector-Aquarium-Analytic/dp/B01IUVHB8E/ref=asc_df_B01IUVHB8E/?tag=hyprod-20&linkCode=df0&hvadid=198093101467&hvpos=&hvnetw=g&hvrand=18305154733223653233&hvpone=&hvptwo=&hvqmt=&hvdev=c&hvdvcmdl=&hvlocint=&hvlocphy=9031341&hvtargid=pla-384674250225&psc=1" >here</a>

#### **Motor Shield**

Adafruit motorshield V2 found <a href="https://www.amazon.com/Gikfun-Peristaltic-Connector-Aquarium-Analytic/dp/B01IUVHB8E/ref=asc_df_B01IUVHB8E/?tag=hyprod-20&linkCode=df0&hvadid=198093101467&hvpos=&hvnetw=g&hvrand=18305154733223653233&hvpone=&hvptwo=&hvqmt=&hvdev=c&hvdvcmdl=&hvlocint=&hvlocphy=9031341&hvtargid=pla-384674250225&psc=1" >here</a>

#### **Relay Module**

5V 8 relay module found <a href="https://www.amazon.com/dp/B00DR9SE4A/ref=sspa_dk_detail_1?pd_rd_i=B00E0NTPP4&pd_rd_w=hdnFg&pf_rd_p=085568d9-3b13-4ac1-8ae4-24a26c00cb0c&pd_rd_wg=mp9GA&pf_rd_r=GVMSVT2V4HN321XJTXPA&pd_rd_r=eac3f50c-a3f2-43e9-8b71-0fdae9ee84f2&spLa=ZW5jcnlwdGVkUXVhbGlmaWVyPUExOEtZMzRUNEQzVDEyJmVuY3J5cHRlZElkPUEwNTM0NjUyMzFSRE5aRlhNNktNQyZlbmNyeXB0ZWRBZElkPUExMDA5MDk3M1NDQUxaTFAzQ1Y3MyZ3aWRnZXROYW1lPXNwX2RldGFpbCZhY3Rpb249Y2xpY2tSZWRpcmVjdCZkb05vdExvZ0NsaWNrPXRydWU&th=1" >here</a>

#### **Sensors**

- 5V pressure transducer found <a href="https://www.amazon.com/dp/B07KJHRPLG?psc=1&smid=A2K5DI8VX12AN1&ref_=chk_typ_imgToDp" >here</a>
- 5V digital temperature sensor found <a href="https://www.amazon.com/GAOHOU-DS18B20-Waterproof-Digital-Temperature/dp/B07YS9CKL6/ref=sr_1_11?dchild=1&keywords=arduino+temperature+sensor&qid=1619589863&s=industrial&sr=1-11" >here</a>
- 5V Adafruit magnetometer found <a href="https://www.adafruit.com/product/4022" >here</a>

# **Bioreactor Control System Program Library**

## **All Systems Ready**

##### **bio_reactor_control_system**
This program encapsulates all the individual programs listed below to control the bioreactor. 

## 
## **Subsystems Testing**

### **Motor Shield And Pumps**

##### **adafruit_motor_shield**
This program uses the adafruit motor shield library to send PWM signals to a DC motor (up to 4) and is the main source code this is used in both 
[chamber_pressure_control](#chamber_pressure_control) and [chamber_flow_control](#chamber_flow_control) which both use 12V brushed DC motors for the air compressor and peristaltic pump respectively.

##### **chamber_pressure_control**
This program uses PID control to pressurize the bioreactor to a specified target pressure. PID is implemented by acquiring the chamber pressure reading as described in [pressure_sensor_reading](#pressure_sensor_reading). Once the target pressure is met the air compressor will shut off, then the solenoid valve that is inline with the air compressor will go from open to closed which will seal the bioreactor at the target pressure. 

##### **chamber_flow_control**
This program inflicts a shear stress on the stem cells in the bioreactor by pumping an aqueous solution through the chamber with the peristaltic pump. The peristaltic pump was calibrated by measuring various PWM values and their corresponding flow rates. The user has the ability to vary the PWM signal from 0-255 which will correspond to a flow rate between 0-0.1L/min respectively. 

### **Sensor Readings**
All sensors including the touch screen dispay and SD data logger operate at a baud rate of 9600.

##### **data_logger**
This program writes all the sensor readings; described below, to an SD card in .txt format with the SD data logger module. As long as the bioreactor is active, the module will record all sensor readings and once it becomes deactivated, the module will stop the recording process. 

##### **pressure_sensor_reading**
This program gets the reading from the pressure transducer. The sensor is analog and has a linear relationship between voltage and pressure which is described as follows:

sensor_psi_value = ((sensor_analog_value - pressureMin) * pressuretransducermaxPSI) / (pressureRange)

where:
- sensor_analog_value: raw sensor reading
- pressureMin : **analog** reading of pressure transducer at 0psi
- pressureMax  : **analog** reading of pressure transducer at 10psi
- pressureRange : pressureMax - pressureMin
- pressuretransducermaxPSI : psi value of transducer being used


##### **temperature_sensor_reading**
This program gets the reading from the temperature sensor. A maximum temperature of 50 degrees celcius is set as the cutoff temperature produced by the heating of the magnetic field generation system. If the temperature breaches the maximum temperature, the magnetic field system will automatically shut off by disconnecting its power source through [relay_control](#relay_control). The sensor is digital and had a signal conditioning board that converted the digital reading to a reading in celcius. 

##### **magnetic_field_sensor_reading**
This program gets the reading from the magnetometer sensor. The sensor is located on the top face of the bioreactor and measures the penetrating magentic field on the stem cells. This will provide real time information which will allow the user to make adjustments as needed to stimulate the stem cells under various loading conditions.

### **Relay Control**
##### **relay_control**
This program activates or deactivates any of the 8 relays in the bioreactor control center. The 5V relay board is powered independently of the SBC which is ideal so that it is not possible to damage the SBC. The user has the ability to turn on/off any system at any time for convience or experimental reasons but is recommended to use the emergency off switch located on top of the control center to quickly disable the entire relay module which will disconnet power to air compressor, peristaltic pump, solenoid valve, magnetic field system, electric field system and the cooling fan. 

## 

# GUI 



How to make a Table
| Field 1        | Field 2               | Field 1                                                        |
| ----------     | --------------------- | ---------------------------------------------------------- |
| info 1 | info 2| info 3                     |


<a href="insert https link here" >here</a>
