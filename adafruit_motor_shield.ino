#include <Wire.h>
#include <Adafruit_MotorShield.h>
#include "utility/Adafruit_MS_PWMServoDriver.h"
#include <Nextion.h>

Adafruit_MotorShield AFMS = Adafruit_MotorShield(); 
Adafruit_DCMotor *myMotor = AFMS.getMotor(3);

const int minPumpSpeed = 0;
const int maxPumpSpeed = 255;
const int time_to_pressurize = 50;
const int seconds_to_miliseconds = 1000;
const int baudRate = 9600;
const int sensorreadDelay = 1000;

const int relayPin_1 = 12;
const int relayPin_2 = 13;

NexButton b1 = NexButton(2,5,"b1");
NexButton b2 = NexButton(2,6,"b2");

NexTouch *nex_listen_list[] = 
{
  &b1,
  &b2
  };

void setup(void)
{
  Serial.begin(baudRate);
  AFMS.begin();
  myMotor->setSpeed(minPumpSpeed);
  myMotor->run(FORWARD);
  digitalWrite(relayPin_1, HIGH);
  digitalWrite(relayPin_2, HIGH);
  pinMode(relayPin_1, OUTPUT);
  pinMode(relayPin_2, OUTPUT);
  b1.attachPop(b1PopCallback);
  b2.attachPop(b2PopCallback);
  delay(sensorreadDelay);
}

void loop(void) {
  nexLoop(nex_listen_list);
  if (millis() < time_to_pressurize * seconds_to_miliseconds) {
  myMotor->setSpeed(maxPumpSpeed);
  myMotor->run(FORWARD);
  }
}

void b1PopCallback(void *ptr) {
  digitalWrite(relayPin_1, LOW);
}

void b2PopCallback(void *ptr) {
  digitalWrite(relayPin_1, HIGH);
}

void writeToByte(String display_string, int display_value) {
  Serial.print(display_string); //prints value from previous line to serial
  Serial.print(display_value); //can only send integers, must multiply 1000 and make sure it is type int
  Serial.write(0xff);
  Serial.write(0xff);
  Serial.write(0xff);
}
