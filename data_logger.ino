#include <SD.h>
#include <SPI.h>
#include <OneWire.h>
#include <DallasTemperature.h>
#include "Adafruit_MLX90393.h"
#include <Wire.h>

File myFile;
int pinCS = 53; // Pin 10 on Arduino Uno
String fileName = "test.txt";

// Pressure Sensor
const int pressureInput = A1; //select the analog input pin for the pressure transducer
const int pressureZero = 102.4; //analog reading of pressure transducer at 0psi
const int pressureMax = 921.6; //analog reading of pressure transducer at 10psi
const int pressureRange = pressureMax - pressureZero; //analog reading of pressure transducer at 10psi
const int pressuretransducermaxPSI = 10; //psi value of transducer being used
float target_analog_value;
float pressureAnalogValue;
float pressureValue = 0; //variable to store the value coming from the pressure transducer
const int zeroPressure = 0;
float pressureTarget = 0.29;
long int pressureTargetInt = int(pressureTarget * 1000);
float defaultPressureTargetAnalog = (pressureTarget * pressureRange / pressuretransducermaxPSI) + pressureZero; //conversion equation to convert psi to analog reading


// Temperature Sensor
const int temperatureInput = 7; // digital pin 7
OneWire oneWire(temperatureInput);
DallasTemperature temperature_sensor(&oneWire);
float temperatureValue = 0; //variable to store the value coming from the temperature sensor
//int temperatureValueInt = temperatureValue * 1000;
//const int temperatureRoomInt = 32 * 1000;
//const int temperatureMaxInt = 80 * 1000;
const int baudRate = 9600; //constant integer to set the baud rate for serial monitor
const int sensorreadDelay = 250; //constant integer to set the sensor read delay in milliseconds
float Celsius = 0;

// Magnetometer Sensor
Adafruit_MLX90393 magnetometer_sensor = Adafruit_MLX90393();
const int MLX90393_CS = 10;
float magneticValue = 0;
//const int magneticZero = 0;
//const long int magneticMaxInt = 20 * 1000;


void setup() {

  Serial.begin(baudRate);

  // SD Card Initialization
  pinMode(pinCS, OUTPUT);
  if (SD.begin())
  {
    Serial.println("SD card is ready to use.");
  } else
  {
    Serial.println("SD card initialization failed");
    return;
  }

  // Create/Open file
  myFile = SD.open(fileName, FILE_WRITE);

  // if the file opened okay, write to it:
  if (myFile) {
    Serial.println("Writing to file...");
    // Write to file
    myFile.println("Testing text 1, 2 ,3...");
    myFile.close(); // close the file
    Serial.println("Done.");
  }
  // if the file didn't open, print an error:
  else {
    Serial.print("error opening file ");
    Serial.println(fileName);
  }


  float pressureAnalogValue;
  pressureAnalogValue = analogRead(pressureInput);
  pressureValue = ((pressureAnalogValue - pressureZero) * pressuretransducermaxPSI) / (pressureMax - pressureZero); //conversion equation to convert analog reading to psi

  // Temperature Sensor
  temperature_sensor.begin();

  // Magnetometer
  magnetometer_sensor.begin_I2C(); // Begin I2C comm
  magnetometer_sensor.setGain(MLX90393_GAIN_2_5X); // Set gain
  magnetometer_sensor.setResolution(MLX90393_X, MLX90393_RES_19); // Set resolution, per axis
  magnetometer_sensor.setOversampling(MLX90393_OSR_2); // Set oversampling
  magnetometer_sensor.setFilter(MLX90393_FILTER_6); // Set digital filtering
}
void loop() {
  while (Celsius < 250) {
    temperature_sensor.requestTemperatures();
    Celsius = temperature_sensor.getTempCByIndex(0);
    Serial.print(millis());
    Serial.print(",");
    Serial.println(Celsius);

    float z;
    sensors_event_t event;
    magnetometer_sensor.getEvent(&event);
    magneticValue = event.magnetic.z * 0.01;

    //  Print to Serial monitor
    Serial.print("Time (mu-s):");
    Serial.print(millis());
    Serial.print(",");
    Serial.print("Pressure (psi): ");
    Serial.print(pressureValue);
    Serial.print(",");
    Serial.print("Temperature (Celcius): ");
    Serial.print(Celsius);
    Serial.print(",");
    Serial.print("magneticValue (Gauss): ");
    Serial.println(magneticValue);

    myFile = SD.open(fileName, FILE_WRITE);
    if (myFile) {
      Serial.println("Writing to file...");
      myFile.print(millis());
      myFile.print(",");
      myFile.println(Celsius);
      myFile.close(); // close the file
    }
    // if the file didn't open, print an error:
    else {
      Serial.print("error opening ");
      Serial.println(fileName);
    }
    delay(3000);
  }
  Serial.println("50 deg met, trial complete");
}
