#include <doxygen.h>
#include <NexButton.h>
#include <NexCheckbox.h>
#include <NexConfig.h>
#include <NexCrop.h>
#include <NexDualStateButton.h>
#include <NexGauge.h>
#include <NexGpio.h>
#include <NexHardware.h>
#include <NexHotspot.h>
#include <NexNumber.h>
#include <NexObject.h>
#include <NexPage.h>
#include <NexPicture.h>
#include <NexProgressBar.h>
#include <NexRadio.h>
#include <NexRtc.h>
#include <NexScrolltext.h>
#include <NexSlider.h>
#include <NexText.h>
#include <NexTimer.h>
#include <Nextion.h>
#include <NexTouch.h>
#include <NexUpload.h>
#include <NexVariable.h>
#include <NexWaveform.h>

#include <SD.h>
#include <SPI.h>
#include <OneWire.h>
#include <DallasTemperature.h>
#include "Adafruit_MLX90393.h"
#include <Adafruit_MotorShield.h>
#include "utility/Adafruit_MS_PWMServoDriver.h"
#include <Wire.h>

// System setup
const int sensor_read_delay = 5000; //constant integer to set the sensor read delay in milliseconds
const int start_up_delay = 100; //constant integer to set the sensor read delay in milliseconds
const int SD_read_delay = 100; //constant integer to set the sensor read delay in milliseconds
const int bioreactor_initialize_delay = 1000; //constant integer to set the sensor read delay in milliseconds
const int bioreactor_start_up_delay = 1000; //constant integer to set the sensor read delay in milliseconds
const int bioreactor_shut_down_delay = 1000; //constant integer to set the sensor read delay in milliseconds
const int baudRate = 9600; //constant integer to set the baud rate for Serial2 monitor
const int seconds_to_miliseconds = 1000;
bool bioreactor_is_on = false;
bool pressure_is_on = false;
bool flowTargetUpdated = false;
bool flowUpdated = false;
bool pressUpdated = false;

// Pressure Sensor
const int pressureInput = A1  ; //select the analog input pin for the pressure transducer
const int pressureZero = 102.4; //analog reading of pressure transducer at 0psi
const int pressureMax = 921.6; //analog reading of pressure transducer at 10psi
const int pressureRange = pressureMax - pressureZero; //analog reading of pressure transducer at 10psi
const int pressuretransducermaxPSI = 10; //psi value of transducer being used
float pressureAnalogValue;
float pressureValue = 0; //variable to store the value coming from the pressure transducer
const int zeroPressure = 0;
//float defaultPressureTargetAnalog = (pressureTarget * pressureRange / pressuretransducermaxPSI) + pressureZero; //conversion equation to convert psi to analog reading

// Compressor Control (PID with pressure transducer)
uint8_t motor_direction;
float control_signal;
int pressure = 0;
long previous_time = 0.0;
int previous_error = 0;
float integral_error = 0;

// Magnetometer Sensor
Adafruit_MLX90393 magnetometer_sensor = Adafruit_MLX90393();
#define MLX90393_CS 10
float magneticValue = 0;
int magneticValueInt = 0;
const int magneticZero = 0;
const long int magneticMaxInt = 20 * 1000;
const float micro_tesla_to_gauss = 0.01;

// Temperature Sensor
const int temperatureInput = 7; // digital pin 7
OneWire oneWire(temperatureInput);
DallasTemperature temperature_sensor(&oneWire);
float temperatureValue = 0; //variable to store the value coming from the temperature sensor
long int temperatureValueInt = temperatureValue * 1000;

// Motor Shield (Compressor & Pump DC Motors)
Adafruit_MotorShield AFMS = Adafruit_MotorShield();
Adafruit_DCMotor *compressor = AFMS.getMotor(1);
Adafruit_DCMotor *pump = AFMS.getMotor(2);

const int minPumpSpeed = 0;
const int maxPumpSpeed = 255;

// Relays
const int relay_compressor = 35;
const int relay_solenoid = 33;
const int relay_pump = 37;
const int relay_magnetic = 43;
const int relay_electric = 41;
const int relay_fan = 39;

bool relay_compressor_state = false;
bool relay_solenoid_state = false;
bool relay_pump_state = false;
bool relay_magnetic_state = false;
bool relay_electric_state = false;
bool relay_fan_state = false;

// SD Card
File dataFile;
File root;
int pinCS = 53;
char buffer[100] = {0};
String name_of_file(buffer);

// Pump Control (calibrated PWM vs. flow rate)
uint32_t flow_target_user = 25; // mL/min

// GUI

// GUI - Pressure variables
String pressureValueString = "health.pressureValue.val=";
String pressureTargetString = "pressure.pressureTarget.val=";
String kpPressureValString = "pressure.kpPressureVal.val=";
String kdPressureValString = "pressure.kdPressureVal.val=";
String kiPressureValString = "pressure.kiPressureVal.val=";
String getPressTarString = "pressure.getPressTar.val=";
static uint8_t pressure_plot_data;
NexWaveform pressPlot = NexWaveform(4, 24, "pressPlot");
uint32_t pressure_target_user;
uint32_t kp_user;
uint32_t ki_user;
uint32_t kd_user;

// GUI - Flow variables
String flowValueString = "health.flowValue.val=";
String flowTargetString = "flow.flowTarget.val=";
static uint8_t flow_plot_data;
NexWaveform flowPlot = NexWaveform(5, 10, "flowPlot");

// GUI - Magnetometer variables
String magneticValueString = "health.magneticValue.val=";
static uint8_t mag_plot_data;
NexWaveform magPlot = NexWaveform(6, 14, "magPlot");

// GUI - Temperature Sensor variables
String temperatureValueString = "health.tempValue.val=";
static uint8_t temp_plot_data;
NexWaveform tempPlot = NexWaveform(6, 13, "s0");

// GUI - Buttons, Values, Waveforms and Text
NexButton start_bioreactor = NexButton(0, 1, "start");
NexButton stop_bioreactor = NexButton(0, 2, "stop");

NexButton pressureON = NexButton(3, 15, "pressureON");
NexButton pressureOFF = NexButton(3, 14, "pressureOFF");
NexButton valveON = NexButton(2, 17, "valveON");
NexButton valveOFF = NexButton(2, 16, "valveOFF");
NexButton flowON = NexButton(3, 17, "flowON");
NexButton flowOFF = NexButton(3, 16, "flowOFF");
NexButton magnetON = NexButton(3, 19, "magnetON");
NexButton magnetOFF = NexButton(3, 18, "magnetOFF");
NexButton electricON = NexButton(3, 24, "electricON");
NexButton electricOFF = NexButton(3, 22, "electricOFF");
NexButton fanON = NexButton(3, 27, "fanON");
NexButton fanOFF = NexButton(3, 25, "fanOFF");

NexButton updatePressure = NexButton(4, 19, "pressureON");
NexButton defaultPress = NexButton(4, 10, "defaultPress");
NexButton updateFlow = NexButton(5, 6, "updateFlow");
NexButton defaultFlow = NexButton(5, 5, "defaultFlow");

// PID from user
NexNumber getPressTar = NexNumber (4, 21, "getPressTar");
NexNumber getKp = NexNumber (4, 20, "getKp");
NexNumber getKi = NexNumber (4, 22, "getKi");
NexNumber getKd = NexNumber (4, 23, "getKd");

// Flow target from user
NexNumber getFlowTar = NexNumber (4, 21, "getFlowTar");

// Sensor Values
NexNumber pressureValINT = NexNumber (3, 28, "pressureValINT");
NexNumber flowValINT = NexNumber (3, 30, "flowValINT");
NexNumber magneticValINT = NexNumber (3, 31, "magneticValINT");
NexNumber tempValINT = NexNumber (3, 32, "tempValINT");

// SD card
NexText SDCardStatus = NexText(0, 5, "SDCardStatus");
NexText fileName = NexText(7, 4, "fileName");
NexText dataList = NexText(7, 3, "dataList");
String SDCardStatusString = "SDCardStatus.txt=";
String dataListString = "data.dataList.txt=";

NexTouch *nex_listen_list[] =
{
  &start_bioreactor,
  &stop_bioreactor,
  &pressureON,
  &pressureOFF,
  &valveON,
  &valveOFF,
  &flowON,
  &flowOFF,
  &magnetON,
  &magnetOFF,
  &electricON,
  &electricOFF,
  &fanON,
  &fanOFF,
  &updatePressure,
  &defaultPress,
  &updateFlow,
  &defaultFlow,
  &SDCardStatus,
  &fileName,
  &dataList,
  NULL
};

// Default Values
float default_target_psi_value = 0.29;
float default_target_analog_value = (default_target_psi_value * pressureRange / pressuretransducermaxPSI) + pressureZero; //conversion equation to convert psi to analog reading
float threshold = 0.05 * default_target_psi_value;
float default_kp = 0.3;
float default_ki = 0.1;
float default_kd = 0.1;
float default_flow_rate = 25.0; // mL/min

// Setting Default Values
float target_analog_value = default_target_analog_value;
float kp = default_kp;
float ki = default_ki;
float kd = default_kd;
float flow_rate = default_flow_rate;

void setup() //setup routine, runs once when system turned on or reset
{
  Serial2.begin(baudRate);
  SDCardStatus.setText("Bioreactor initializing ...");
  delay(bioreactor_initialize_delay);

  // Initialize SD Card
  SDCardStatus.setText("Initializing SD card...");
  delay(bioreactor_initialize_delay);
  //  pinMode(pinCS, OUTPUT);
  if (SD.begin(pinCS))
  {
    SDCardStatus.setText("SD card detected!");
    delay(bioreactor_initialize_delay);
    SDCardStatus.setText("Loading SD files...");
    delay(bioreactor_initialize_delay);
    root = SD.open("/");
    printDirectory(root, 0);
  }
  else
  {
    SDCardStatus.setText("SD card not detected.");
  }
  delay(bioreactor_initialize_delay);

  // Initialize temperature sensor
  SDCardStatus.setText("Initializing temperature sensor...");
  delay(bioreactor_initialize_delay);
  temperature_sensor.begin();
  SDCardStatus.setText("Temperature sensor initialized!");
  delay(bioreactor_initialize_delay);

  // Initialize magnetometer sensor
  SDCardStatus.setText("Initializing magnetometer sensor...");
  delay(bioreactor_initialize_delay);
  //  magnetometer_sensor.begin_I2C(); // Begin I2C comm

  if (! magnetometer_sensor.begin_I2C()) {
    SDCardStatus.setText("Failed to initialize magnetometer sensor...");
  }
  else {
    magnetometer_sensor.setGain(MLX90393_GAIN_2_5X); // Set gain
    magnetometer_sensor.setResolution(MLX90393_X, MLX90393_RES_19);
    magnetometer_sensor.setResolution(MLX90393_Y, MLX90393_RES_19);
    magnetometer_sensor.setResolution(MLX90393_Z, MLX90393_RES_16);
    magnetometer_sensor.setOversampling(MLX90393_OSR_2); // Set oversampling
    magnetometer_sensor.setFilter(MLX90393_FILTER_6); // Set digital filtering
    SDCardStatus.setText("Magnetometer sensor initialized!");
  }
  delay(bioreactor_initialize_delay);

  // All sensors initialized
  SDCardStatus.setText("Sensors initialization complete!");
  delay(bioreactor_initialize_delay);

  //  Initialize relays
  SDCardStatus.setText("Initializing relays...");
  delay(bioreactor_initialize_delay);
  digitalWrite(relay_compressor, HIGH);
  digitalWrite(relay_solenoid, HIGH);
  digitalWrite(relay_pump, HIGH);
  digitalWrite(relay_magnetic, HIGH);
  digitalWrite(relay_electric, HIGH);
  digitalWrite(relay_fan, HIGH);
  pinMode(relay_compressor, OUTPUT);
  pinMode(relay_solenoid, OUTPUT);
  pinMode(relay_pump, OUTPUT);
  pinMode(relay_magnetic, OUTPUT);
  pinMode(relay_electric, OUTPUT);
  pinMode(relay_fan, OUTPUT);
  SDCardStatus.setText("Relays initialized!");
  delay(bioreactor_initialize_delay);

  // Initialize Motor Shield
  SDCardStatus.setText("Initializing motor shield...");
  delay(bioreactor_initialize_delay);
  AFMS.begin();
  compressor->setSpeed(minPumpSpeed);
  compressor->run(FORWARD);
  SDCardStatus.setText("Motor shield initialized!");
  delay(bioreactor_initialize_delay);

  //  Initialize buttons in GUI
  SDCardStatus.setText("Initializing GUI...");
  delay(bioreactor_initialize_delay);

  start_bioreactor.attachPop(start_bioreactorPopCallback);
  stop_bioreactor.attachPop(stop_bioreactorPopCallback);

  pressureON.attachPop(pressureONPopCallback);
  pressureOFF.attachPop(pressureOFFPopCallback);
  flowON.attachPop(flowONPopCallback);
  flowOFF.attachPop(flowOFFPopCallback);
  magnetON.attachPop(magnetONPopCallback);
  magnetOFF.attachPop(magnetOFFPopCallback);
  electricON.attachPop(electricONPopCallback);
  electricOFF.attachPop(electricOFFPopCallback);
  valveON.attachPop(valveONPopCallback);
  valveOFF.attachPop(valveOFFPopCallback);
  fanON.attachPop(fanONPopCallback);
  fanOFF.attachPop(fanOFFPopCallback);
  updatePressure.attachPop(updatePressurePopCallback);
  defaultPress.attachPop(defaultPressPopCallback);
  updateFlow.attachPop(updateFlowPopCallback);
  defaultFlow.attachPop(defaultFlowPopCallback);
  SDCardStatus.attachPop(SDCardStatusPopCallback);
  fileName.attachPop(fileNamePopCallback);
  dataList.attachPop(dataListPopCallback);
  SDCardStatus.setText("GUI Initialized!");

  delay(bioreactor_initialize_delay);
  SDCardStatus.setText("Bioreactor now in standby mode.");

  // Sending Default Values To Display
  writeToByte(pressureTargetString, default_target_psi_value * 1000);
  writeToByte(kpPressureValString, kp * 1000);
  writeToByte(kdPressureValString, ki * 1000);
  writeToByte(kiPressureValString, kd * 1000);
  writeToByte(flowTargetString, flow_rate * 1000);
}

void loop()
{
  nexLoop(nex_listen_list);
  if (bioreactor_is_on) {

    // Temperature Sensor
    temperatureData();
    delay(100);

    // Magnetometer Sensor
    magnetometerData();
    delay(100);

    // Pressure Sensor
    //    pressureData();
    pressureAnalogValue = analogRead(pressureInput);
    pressureValue = ((pressureAnalogValue - pressureZero) * pressuretransducermaxPSI) / (pressureMax - pressureZero); //conversion equation to convert analog reading to psi

    delay(100);
    if (pressureValue < 0) {
      pressureValue = 0;
    }
    writeToByte(pressureValueString, pressureValue * 1000);

    uint32_t pressureValueInt = uint32_t(pressureValue);
    pressureValINT.setValue(pressureValueInt);
    pressure_plot_data = uint8_t(pressureValue);
    uint8_t pressure_max_plot_data = 2;
    pressPlot.addValue(0, pressure_plot_data);
    pressPlot.addValue(2, pressure_max_plot_data);
    delay(100);

    // Actuators
    if (pressure_is_on) {
      // PID
      digitalWrite(relay_solenoid, LOW);
      if (pressureAnalogValue >= target_analog_value ) {
        control_signal = 0;
        digitalWrite(relay_solenoid, HIGH);
      }
      else {
        control_signal = 30  ;
        }
      // send control signal to compressor
      compressor->setSpeed(control_signal);
      compressor->run(motor_direction);
      delay(100);
    }
    // PID
//      digitalWrite(relay_solenoid, LOW);
//      long current_time = micros();
//      long delt_time = ((float)(current_time - previous_time)) / 1.0e6;
//      previous_time = current_time;
//      int error = target_analog_value - pressureAnalogValue; // kp
//      pressureAnalogValue = analogRead(pressureInput);
//      pressureValue = ((pressureAnalogValue - pressureZero) * pressuretransducermaxPSI) / (pressureMax - pressureZero); //conversion equation to convert analog reading to psi
//      nexLoop(nex_listen_list);
//      float de_dt = (error - previous_error) / delt_time; // kd
//      integral_error = integral_error + error * delt_time; // ki
//      float u = kp * error + kd * de_dt + ki * integral_error; // control signal
//
//      control_signal = fabs(u);
//      if (control_signal > 255) {
//        control_signal = 255;
//      }
//      if (u < 0) {
//        motor_direction = BACKWARD;
//      }
//      else {
//        motor_direction = FORWARD;
//      }
//      if (abs(error) <= threshold) {
//        control_signal = 0;
//        digitalWrite(relay_solenoid, HIGH);
//      }
//      // send control signal to compressor
//      compressor->setSpeed(control_signal);
//      compressor->run(motor_direction);
//      //        pressureData();
//      delay(100);

    float flow_to_pwm =  2880 * (flow_rate) + 29.13;
    pump->setSpeed(flow_to_pwm);
    pump->run(FORWARD);

    delay(100);
    writeToByte(flowValueString, flow_rate * 1000);
    flow_plot_data = uint8_t(flow_rate);
    uint8_t flow_max_plot_data = 100;
    flowPlot.addValue(0, flow_plot_data);
    flowPlot.addValue(2, flow_max_plot_data);

    // Data logging
    memset(buffer, 0, sizeof(buffer));
    fileName.getText(buffer, sizeof(buffer));
    String name_of_file(buffer);
    dataFile = SD.open(name_of_file, FILE_WRITE);
    if (dataFile) {
      dataFile.print(millis());
      dataFile.print(",");
      dataFile.print(pressureValue);
      dataFile.print(",");
      dataFile.print(temperatureValue);
      dataFile.print(",");
      dataFile.println(magneticValue);
      dataFile.close(); // close the file
      SDCardStatus.setText("Writting to file...");
      delay(sensor_read_delay);
    }
    else {
      SDCardStatus.setText("Error 2: Could not open file.");
      delay(sensor_read_delay);
    }
  }
}

void start_bioreactorPopCallback(void *ptr) {
  bioreactor_is_on = true;

  memset(buffer, 0, sizeof(buffer));
  fileName.getText(buffer, sizeof(buffer));
  String name_of_file(buffer);
  textToByte(SDCardStatusString, name_of_file);
  delay(bioreactor_start_up_delay);

  // Begin data logging
  dataFile = SD.open(name_of_file, FILE_WRITE);
  if (dataFile) {
    SDCardStatus.setText("Creating New Data File...");
    delay(bioreactor_start_up_delay);
    dataFile.println("Time (ms), Pressure (psi), Temperature(C), Magnetic Field (Gauss)");
    dataFile.close();
    SDCardStatus.setText("New File Created For Experiment");
    delay(bioreactor_start_up_delay);
  }
  else {
    SDCardStatus.setText("Error 1: Could not create new file.");
    delay(bioreactor_start_up_delay);
  }

  // Activate selected relays by user
  if (relay_magnetic_state) {
    digitalWrite(relay_magnetic, LOW);
  }
  if (relay_compressor_state) {
    digitalWrite(relay_compressor, LOW);
  }
  if (relay_solenoid_state) {
    digitalWrite(relay_solenoid, LOW);
  }
  if (relay_pump_state) {
    digitalWrite(relay_pump, LOW);
  }
  if (relay_electric_state) {
    digitalWrite(relay_electric, LOW);
  }
  if (relay_fan_state) {
    digitalWrite(relay_fan, LOW);
  }
  SDCardStatus.setText("Bioreactor Is Now Active!");
  delay(bioreactor_start_up_delay);
}

void stop_bioreactorPopCallback(void *ptr) {
  bioreactor_is_on = false;
  SDCardStatus.setText("Deactivating All Bioreactor Systems...");
  delay(bioreactor_shut_down_delay);
  digitalWrite(relay_compressor, HIGH);
  digitalWrite(relay_solenoid, HIGH);
  digitalWrite(relay_pump, HIGH);
  digitalWrite(relay_magnetic, HIGH);
  digitalWrite(relay_electric, HIGH);
  digitalWrite(relay_fan, HIGH);
  delay(bioreactor_shut_down_delay);
  relay_compressor_state = false;
  relay_solenoid_state = false;
  relay_pump_state = false;
  relay_magnetic_state = false;
  relay_electric_state = false;
  relay_fan_state = false;
  delay(bioreactor_shut_down_delay);
  SDCardStatus.setText("Bioreactor Successfully Deactivated.");
  delay(bioreactor_shut_down_delay);
  SDCardStatus.setText("Bioreactor Now In Standby Mode.");
}

//void pressureData(void) {
//  pressureAnalogValue = analogRead(pressureInput);
//  pressureValue = ((pressureAnalogValue - pressureZero) * pressuretransducermaxPSI) / (pressureMax - pressureZero); //conversion equation to convert analog reading to psi
//
//  delay(100);
//  if (pressureValue < 0) {
//    pressureValue = 0;
//  }
//  writeToByte(pressureValueString, pressureValue * 1000);
//
//  uint32_t pressureValueInt = uint32_t(pressureValue);
//  pressureValINT.setValue(pressureValueInt);
//  pressure_plot_data = uint8_t(pressureValue);
//  uint8_t pressure_max_plot_data = 2;
//  pressPlot.addValue(0, pressure_plot_data);
//  pressPlot.addValue(2, pressure_max_plot_data);
//}

void magnetometerData(void) {
  float z;
  sensors_event_t event;
  magnetometer_sensor.getEvent(&event);
  magneticValue = event.magnetic.z * micro_tesla_to_gauss; // in microTesla
  uint32_t magneticValueInt = uint32_t (magneticValue);
  //  magneticValINT.setValue(magneticValueInt);
  mag_plot_data = uint8_t(magneticValue);
  uint8_t mag_max_plot_data = 30;
  magPlot.addValue(0, mag_plot_data);
  magPlot.addValue(2, mag_max_plot_data);
  writeToByte(magneticValueString, magneticValue * 1000);
}

void temperatureData(void) {
  temperature_sensor.requestTemperatures();
  temperatureValue = temperature_sensor.getTempCByIndex(0);
  uint32_t temperatureValueInt = uint32_t (temperatureValue);
  //  tempValINT.setValue(temperatureValueInt);
  temp_plot_data = uint8_t(temperatureValue);
  uint8_t temp_max_plot_data = 50;
  tempPlot.addValue(0, temp_plot_data);
  tempPlot.addValue(2, temp_max_plot_data);
  writeToByte(temperatureValueString, temperatureValue * 1000);
}

void pressureONPopCallback(void *ptr) {
  pressure_is_on = true;
  if (bioreactor_is_on) {
    digitalWrite(relay_compressor, LOW);
  }
  else {
    relay_compressor_state = true;
  }
}

void pressureOFFPopCallback(void *ptr) {
  pressure_is_on = false;
  if (bioreactor_is_on) {
    digitalWrite(relay_compressor, HIGH);
  }
  else {
    relay_compressor_state = false;
  }
}

void valveONPopCallback(void *ptr) {
  if (bioreactor_is_on) {
    digitalWrite(relay_solenoid, LOW);
  }
  else {
    relay_solenoid_state = true;
  }
}

void valveOFFPopCallback(void *ptr) {
  if (bioreactor_is_on) {
    digitalWrite(relay_solenoid, HIGH);
  }
  else {
    relay_solenoid_state = false;
  }
}

void flowONPopCallback(void *ptr) {
  if (bioreactor_is_on) {
    digitalWrite(relay_pump, LOW);
  }
  else {
    relay_pump_state = true;
  }
}

void flowOFFPopCallback(void *ptr) {
  if (bioreactor_is_on) {
    digitalWrite(relay_pump, HIGH);
  }
  else {
    relay_pump_state = false;
  }
}

void magnetONPopCallback(void *ptr) {
  if (bioreactor_is_on) {
    digitalWrite(relay_magnetic, LOW);
  }
  else {
    relay_magnetic_state = true;
  }
}

void magnetOFFPopCallback(void *ptr) {
  if (bioreactor_is_on) {
    digitalWrite(relay_magnetic, HIGH);
  }
  else {
    relay_magnetic_state = false;
  }
}

void electricONPopCallback(void *ptr) {
  if (bioreactor_is_on) {
    digitalWrite(relay_electric, LOW);
  }
  else {
    relay_electric_state = true;
  }
}

void electricOFFPopCallback(void *ptr) {
  if (bioreactor_is_on) {
    digitalWrite(relay_electric, HIGH);
  }
  else {
    relay_electric_state = false;
  }
}

void fanONPopCallback(void *ptr) {
  if (bioreactor_is_on) {
    digitalWrite(relay_fan, LOW);
  }
  else {
    relay_fan_state = true;
  }
}

void fanOFFPopCallback(void *ptr) {
  if (bioreactor_is_on) {
    digitalWrite(relay_fan, HIGH);
  }
  else {
    relay_fan_state = false;
  }
}

void updatePressurePopCallback(void *ptr) {
  pressUpdated = true;
  getPressTar.getValue(&pressure_target_user);
  getKp.getValue(&kp_user);
  getKi.getValue(&ki_user);
  getKd.getValue(&kd_user);
  target_analog_value = float(((pressure_target_user / 1000) * pressureRange / pressuretransducermaxPSI) + pressureZero); //conversion equation to convert psi to analog reading
  kp = float(kp_user / 1000);
  ki = float(ki_user / 1000);
  kd = float(kd_user / 1000);
}

void defaultPressPopCallback(void *ptr) {
  target_analog_value = default_target_analog_value;
  kp = default_kp;
  ki = default_ki;
  kd = default_kd;
  writeToByte(pressureTargetString, default_target_psi_value * 1000);
  writeToByte(kpPressureValString, kp * 1000);
  writeToByte(kdPressureValString, ki * 1000);
  writeToByte(kiPressureValString, kd * 1000);
}

void updateFlowPopCallback(void *ptr) {
  flowUpdated = true;
  getFlowTar.getValue(&flow_target_user);
  flow_rate = float(flow_target_user) / 1000;
}

void defaultFlowPopCallback(void *ptr) {
  flow_rate = default_flow_rate;
  writeToByte(flowTargetString,  flow_rate * 1000);
}

void SDCardStatusPopCallback(void *ptr)
{
  SDCardStatus.setText("default");
}

void fileNamePopCallback(void *ptr)
{
  memset(buffer, 0, sizeof(buffer));
  fileName.getText(buffer, sizeof(buffer));
  String name_of_file(buffer);
}

void dataListPopCallback(void *ptr)
{
  dataList.setText("default");
}

void printDirectory(File dir, int numTabs) {
  String file_names;
  while (true) {
    File entry =  dir.openNextFile();
    if (! entry) {
      break;
    }
    SDCardStatus.setText("Adding files to data logging sheet.");
    delay(SD_read_delay);
    name_of_file = entry.name();
    file_names += name_of_file + "\\r";
    entry.close();
  }
  textToByte(dataListString, file_names);
  delay(bioreactor_initialize_delay);
}

void writeToByte(String display_string, float value) {
  int display_value = int(value);
  Serial2.print(display_string); //prints value from previous line to Serial2
  Serial2.print(display_value); //can only send integers, must multiply 1000 and make sure it is type int
  Serial2.write(0xff);
  Serial2.write(0xff);
  Serial2.write(0xff);
}

void textToByte(String display_string, String value) {
  Serial2.print(display_string); //prints value from previous line to Serial2
  Serial2.write("\"");
  Serial2.print(value);
  Serial2.write("\"");
  Serial2.write(0xff);
  Serial2.write(0xff);
  Serial2.write(0xff);
}

void value_to_waveform(int object_id, float value, int channel) {
  int val_wave = int(value);
  String Tosend = "add ";
  Tosend += object_id;
  Tosend += ",";
  Tosend += channel;
  Tosend += ",";
  Tosend += val_wave;
  Serial2.print(Tosend);
  Serial2.write(0xff);
  Serial2.write(0xff);
  Serial2.write(0xff);
}
