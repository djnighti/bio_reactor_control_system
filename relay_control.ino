#include <doxygen.h>
#include <NexButton.h>
#include <NexCheckbox.h>
#include <NexConfig.h>
#include <NexCrop.h>
#include <NexDualStateButton.h>
#include <NexGauge.h>
#include <NexGpio.h>
#include <NexHardware.h>
#include <NexHotspot.h>
#include <NexNumber.h>
#include <NexObject.h>
#include <NexPage.h>
#include <NexPicture.h>
#include <NexProgressBar.h>
#include <NexRadio.h>
#include <NexRtc.h>
#include <NexScrolltext.h>
#include <NexSlider.h>
#include <NexText.h>
#include <NexTimer.h>
#include <Nextion.h>
#include <NexTouch.h>
#include <NexUpload.h>
#include <NexVariable.h>
#include <NexWaveform.h>

// Relays
const int relay_compressor = 35;
const int relay_solenoid = 33;
const int relay_pump = 37;
const int relay_magnetic = 43;
const int relay_electric = 41;
const int relay_fan = 39;

const int baudRate = 9600;
bool state;
const int sensorreadDelay = 1000; //constant integer to set the sensor read delay in milliseconds

// GUI
NexButton pressureON = NexButton(3,15,"pressureON");
NexButton pressureOFF = NexButton(3,14,"pressureOFF");
NexButton valveON = NexButton(2,17,"valveON");
NexButton valveOFF = NexButton(2,16,"valveOFF");
NexButton flowON = NexButton(3,17,"flowON");
NexButton flowOFF = NexButton(3,16,"flowOFF");
NexButton magneticON = NexButton(3,19,"magneticON");
NexButton magneticOFF = NexButton(3,18,"magneticOFF");
NexButton electricON = NexButton(3,24,"electricON");
NexButton electricOFF = NexButton(3,22,"electricOFF");
NexButton fanON = NexButton(3,27,"fanON");
NexButton fanOFF = NexButton(3,25,"fanOFF");

NexTouch *nex_listen_list[] = 
{
  &pressureON,
  &pressureOFF,
  &valveON,
  &valveOFF,
  &flowON,
  &flowOFF,
  &magneticON,
  &magneticOFF,
  &electricON,
  &electricOFF,
  &fanON,
  &fanOFF
  };

void setup() {
  Serial2.begin(baudRate); // Set Baud rate
  
  //  Initialize relays
  digitalWrite(relay_compressor, HIGH);
  digitalWrite(relay_solenoid, HIGH);
  digitalWrite(relay_pump, HIGH);
  digitalWrite(relay_magnetic, HIGH);
  digitalWrite(relay_electric, HIGH);
  digitalWrite(relay_fan, HIGH);
  pinMode(relay_compressor, OUTPUT);
  pinMode(relay_solenoid, OUTPUT);
  pinMode(relay_pump, OUTPUT);
  pinMode(relay_magnetic, OUTPUT);
  pinMode(relay_electric, OUTPUT);
  pinMode(relay_fan, OUTPUT);
  delay(sensorreadDelay);
  
  //  Initialize buttons in GUI
  pressureON.attachPop(pressureONPopCallback);
  pressureOFF.attachPop(pressureOFFPopCallback);
  flowON.attachPop(flowONPopCallback);
  flowOFF.attachPop(flowOFFPopCallback);
  magneticON.attachPop(magneticONPopCallback);
  magneticOFF.attachPop(magneticOFFPopCallback);
  electricON.attachPop(electricONPopCallback);
  electricOFF.attachPop(electricOFFPopCallback);
  valveON.attachPop(valveONPopCallback);
  valveOFF.attachPop(valveOFFPopCallback);
  fanON.attachPop(fanONPopCallback);
  fanOFF.attachPop(fanOFFPopCallback);
}

void loop() {
  nexLoop(nex_listen_list);   
}

void pressureONPopCallback(void *ptr) {
  digitalWrite(relay_compressor, LOW);
}

void pressureOFFPopCallback(void *ptr) {
  digitalWrite(relay_compressor, HIGH);
}

void valveONPopCallback(void *ptr) {
  digitalWrite(relay_solenoid, LOW);
}

void valveOFFPopCallback(void *ptr) {
  digitalWrite(relay_solenoid, HIGH);
}

void flowONPopCallback(void *ptr) {
  digitalWrite(relay_pump, LOW);
}

void flowOFFPopCallback(void *ptr) {
  digitalWrite(relay_pump, HIGH);
}

void magneticONPopCallback(void *ptr) {
  digitalWrite(relay_magnetic, LOW);
}

void magneticOFFPopCallback(void *ptr) {
  digitalWrite(relay_magnetic, HIGH);
}

void electricONPopCallback(void *ptr) {
  digitalWrite(relay_electric, LOW);
}

void electricOFFPopCallback(void *ptr) {
  digitalWrite(relay_electric, HIGH);
}

void fanONPopCallback(void *ptr) {
  digitalWrite(relay_fan, LOW);
}

void fanOFFPopCallback(void *ptr) {
  digitalWrite(relay_fan, HIGH);
}
